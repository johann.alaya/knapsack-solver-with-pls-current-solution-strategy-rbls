# -*- coding: utf-8 -*-
"""
Éditeur de Spyder

Ceci est un script temporaire.
"""


from PIL import Image
import random
import numpy as np
import math
from gurobipy import *
import os
import time
import csv

def generate_data(filename):
    file = open(filename,'r')
    data=dict()
    k=0
    for line in file.readlines():
        line=line[:-1]
        line=line.split(' ')
        if line[0]=='i' :
            weight=int(line[1])
            v=[]
            for u in range(2,len(line)):
                v.append(int(line[u]))
            data[k]=(weight,v)
            k+=1
        if line[0]=='W' :
            data['W']=int(line[1])
    return data
        


class QTree():
    def __init__(self,sol):
        self.sol=sol
        self.child=dict()
        
        
        
    def lenght(self):
        s=1
        for k,v in self.child.items():
            s+=v.lenght()
        return s
    
    def toList(self):
        l=[]
        l.append(self.sol)
        for k,v in self.child.items():
            l+=v.toList()
        return l
    
    def copy(self):
        copy=QTree(self.sol.copy())
        copy.child=self.child.copy()
        
        return copy
    
    def getchildren(self):
        l=[]
        for k,v in self.child.items():
            l.append(v.sol)
            l+=v.getchildren()
        return l
    
    
        
        
    def update(self,data,newval):
        n=calc_k(data,newval,self.sol)
        #si newval domine la racine
        root_change=False
        if sum(n)==len(n):
            self.sol=newval
            root_change=True
            chosen=True

        if sum(n)==0 :
            return False

            
        
        #teste si newval est dominé par un fils
        for key,value in self.child.items():
            candidate=True
            for i in range(len(key)):
                if (n[i]==0 and key[i]==1):
                    candidate=False
            if candidate :
                if domine(data,value.sol,newval):
                    #valeur meilleure dans l'arbre 
                    return False
            
        #teste si un fils est dominé par newval
        mark=[]
        for key,value in self.child.items():
            candidate=True
            for i in range(len(key)):
                if (n[i]==1 and key[i]==0):
                    candidate=False
            if candidate :
                if domine(data,newval,value.sol) :
                    if not key in mark:
                        mark.append(key)
        for k in mark :
            children=self.child[k].getchildren()
            del self.child[k]
            for v in children:
                self.update(data,v)
                
                        
                        
                        
        if not root_change :
            q=self.child.get((tuple(n)))
            if q==None :
                self.child[tuple(n)]=QTree(newval)
                chosen=True
            else :
                chosen=value.update(data,newval)
        return chosen
                
        
        
                    
            
        
        

        
def calc_k(data,newval,sol):
    vnewval=value(data,newval)
    vsol=value(data,sol)
    k=np.zeros(len(vsol))
    for i in range(len(vsol)):
        if vnewval[i]>vsol[i]:
            k[i]=1
        else :
            k[i]=0
    return k
    
    
        
def value(data,s) :
    val=np.zeros(len(data[0][1]))
    for v in s :
        obj=data[v][1]
        for l in range(len(obj)):
            val[l]+=obj[l]
    return val


def calc_weight(data,x):
    w=0
    for k in x :
        w+=data[k][0]
    return w
    

def domine(data,x,y):
    vx=value(data,x)
    vy=value(data,y)
    weak=False
    for i in range(len(vx)):
        if vx[i]>vy[i] :
            weak=True
        if vx[i]<vy[i]:
            return False
    if weak :
        return True
    return False


def subdict(data,s):
    weight=calc_weight(data,s)
    tp=dict()
    for key,value in data.items():
        if key!='W':
            if key not in s:
                if weight+value[0] < data['W']:
                    tp[key]=value
    return tp
    
def neighbor(data,s):
    res=[]
    for i in range(len(s)):
        temp=s.copy()
        retired=temp.pop(i)
        tp=subdict(data,temp)
        if retired in tp.keys() :
            del tp[retired]
        for k,v in tp.items():
            tmp=temp.copy()
            tmp.append(k)
            tp2=subdict(data,tmp)
            while(len(tp2)>0):
                tmp.append(random.choice(list(tp2.keys())))
                tp2=subdict(data,tmp)
            res.append(tmp)
    return res     

def PLS(data):
    P0=[]
    #initialisation of P0
    while True :
        n=random.randint(0,len(data)-2)
        while n in P0 :
            n=random.randint(0,len(data)-2)
        p=P0.copy()
        p.append(n)
        if calc_weight(data,p) > data['W'] :
            break
        else :
            P0.append(n)
    
    Xe=QTree(P0)
    P=QTree(P0)
    Pa=QTree([])
    
    while len(P.sol) >0 :
        for s in P.toList() :
            nei=neighbor(data,s)
            for v in nei :
                if not domine(data,s,v):
                    success=Xe.update(data,v)
                    if success :
                        success=Pa.update(data,v)
        P=Pa.copy()
        Pa=QTree([])
        
    #print(Xe.toList())
    return Xe.toList()

def OWA(point, w,data):
    #on travaille sur un pb de maximisation donc on trie en ordre décroissant.
    #les points sont représnetés par des listes
    v=value(data,point)
    v.sort()
    np.flip(v)
    r=0
    for i in range(len(v)):
        r+=v[i]*w[i]
    return r

def RandomOWAWeights(n,upperbound,lowerbound=0):
    #retourne des poids au hasard qui satisfont la contrainte OWA (somme totale des poids = 1) pour générer les préférences du décideur
    L=[float(random.randrange(lowerbound,upperbound)) for i in range(n)] #a vérifier si le générateur fonctionne ou si il est évalué juste une fois
    a=sum(L)
    return [L[i]/a for i in range(n)]

def PairwiseMaxRegret(x, y, m, w,data):
    #On cherche les poids d'OWA possible selon les contraintes du modèle qui produisent le plus grand PMR,
    #comme indiqué dans l'article
    #On fait la supposition que x est préféré a y
    m.update()
    m.setObjective(OWA(y,w,data) - OWA(x,w,data),GRB.MAXIMIZE)
    m.optimize()
    return m.objVal
    #return [w[i].X for i in range (len(w))]

def removeNotInlist(L,element):
    L2=L.copy()
    L2.remove(element)
    return L2

def MaxRegret(x,points,m,weightslist,data):
    return np.max([PairwiseMaxRegret(x,y,m,weightslist,data) for y in points])

def ArgmaxRegret(x,points,m,weightslist,data):
    return np.argmax([PairwiseMaxRegret(x,y,m,weightslist,data) for y in points])

def MinMaxRegret(points,m,weightslist,data):
    return np.min([MaxRegret(x,removeNotInlist(points,x),m,weightslist,data) for x in points])

def ArgminMaxRegret(points,m,weightslist,data):
    #Pas sur que prendre la valeur absolue produise TOUJOURS un comportement correct, mais je crois que c'est ok
    return np.argmin([MaxRegret(x,removeNotInlist(points,x),m,weightslist,data) for x in points])

def MinAndArgminMR(points,m,weightslist,data):
    a = [MaxRegret(x,removeNotInlist(points,x),m,weightslist,data) for x in points]
    return np.min(a),np.argmin(a)

def Query(x, y, w,data):
    return OWA(x,w,data) >= OWA(y,w,data)

def Elicitation(n, weightsDecideur, points, threshold, data): #points sera récupéré de PLS, n est la dimension des points
    if len(points)==1:
         return points[0],0
    m = Model("") #on tire aléatoirement les poids qui réprésentent la vision du décideur
    #m.Params.OutputFlag=0
    w = []
    for i in range(n):
        w.append(m.addVar(vtype=GRB.CONTINUOUS, lb=0, name="w%d" % (i+1)))
    m.update()
    m.addConstr(quicksum(w[i] for i in range(n)) == 1, "Contrainte OWA")
    #preferences=[]
    querycount=0
    a,i=MinAndArgminMR(points,m,w,data)
    x=points[i]
    print(a)
    b=1
    wait=input("Pause")
    while a > 0 and b > threshold:
        #on applique l'algo d'élicitation CSS (current solution strategy) décrit dans l'article
        x=points[ArgminMaxRegret(points,m,w,data)]
        y=points[ArgmaxRegret(x,points,m,w,data)]
        querycount+=1
        if Query(x,y,weightsDecideur,data):
            m.addConstr(OWA(x,w,data) >= OWA(y,w,data), "Query "+str(querycount))
            #preferences.append((x,y)) #on considère qu'une paire dans le sens x,y signifie x > y et inversement
        else:
            m.addConstr(OWA(y,w,data) >= OWA(x,w,data), "Query "+str(querycount))
            #preferences.append((y,x))
        m.update()
        b=MinMaxRegret(points,m,w,data)/a #MinMaxRegret normalisé
        print(b)
        wait=input("Pause")
    return x,querycount #on renvoie le modèle du décideur trouvé et les queries

def RBLS(data,weightsDecideur,max_it):
    P0=[]
    queries=0
    #initialisation of P0
    while True :
        n=random.randint(0,len(data)-2)
        while n in P0 :
            n=random.randint(0,len(data)-2)
        p=P0.copy()
        p.append(n)
        if calc_weight(data,p) > data['W'] :
            break
        else :
            P0.append(n)
    #Solution initiale P0
    P = QTree(P0)
    n=len(value(data,P0))
    a=True
    S=P0
    nbtries=0
    while nbtries <= max_it and a :
        ne = neighbor(data,S)
        for v in ne:
            P.update(data,v)
        L=P.toList()
        x,q=Elicitation(n,weightsDecideur,L,0.0001,data)
        queries+=q
        vS=OWA(S,weightsDecideur,data)
        vx=OWA(x,weightsDecideur,data)
        #print(S,vS,x,vx)
        #wait=input("Pause")
        if (vx<=vS): #si le meilleur des voisins est moins bien que la solution courante, c'est un optimum local
            a=False
        else:
            P=QTree(x)
            S=x
    return queries

def main():
    #test elicitation
    current = os.getcwd()
    filename = current + os.path.sep + "data_test3.dat"
    data = generate_data(filename)
    points = PLS(data)
    n=len(value(data,points[0]))
    weightsDecideur = RandomOWAWeights(n,0,1000)
    b=points[np.argmax([OWA(x,weightsDecideur,data) for x in points])]
    print("test",b,OWA(b, weightsDecideur, data))
    wait=input("Pause")
    x,_,_=Elicitation(n,weightsDecideur,points,0.0000001,data)
    print(x, OWA(x, weightsDecideur, data))

    #test RBLS
    #S,points = RBLS(data,weightsDecideur,100)
    #b=points[np.argmax([OWA(x,weightsDecideur,data) for x in points])]
    #print(S, value(data,S),b,value(data,b),points)

    #comparaison
    #chronos=[]
    # for i in range(20):
    #     print("run numero "+str(i))
    #     chrono1=time.time()
    #     points = PLS(data)
    #     n = len(value(data,points[0]))
    #     weightsDecideur=RandomOWAWeights(n, 1000)
    #     _,queries1=Elicitation(n,weightsDecideur,points,0.0001,data)
    #     chrono1=time.time()-chrono1
    #     chrono1="N/A"
    #     queries1="N/A"

    #     chrono2=time.time()
    #     queries2=RBLS(data,weightsDecideur,100000)
    #     chrono2=time.time()-chrono2
    #     chronos.append([chrono1,queries1,chrono2,queries2])

    # cols= ["Time (PLS+Elicitation)", "Number of Queries (PLS+E)", "Time (RBLS)", "Number of Queries (RBLS)"]
    # with open('resultats.csv', 'w') as f:

    #     # using csv.writer method from CSV package
    #     write = csv.writer(f)

    #     write.writerow(cols)
    #     write.writerows(chronos)

if __name__ == '__main__':
    main()
    
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        